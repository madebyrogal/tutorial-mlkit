require('@tensorflow/tfjs-node')
const tf = require('@tensorflow/tfjs')
const loadCSV = require('./load-csv')

function knn(features, labels, predictionPoint, k) {
    const { mean, variance } = tf.moments(features, 0)
    const scaledPredictionPoint = predictionPoint.sub(mean).div(variance.pow(1/2))
    
    return features
        .sub(mean)
        .div(variance.pow(1/2))
        .sub(scaledPredictionPoint)
        .pow(2)
        .sum(1)
        .pow(1/2)
        .expandDims(1)
        .concat(labels, 1)
        .unstack()
        .sort((tensorA, tensorB) => {
            return tensorA.get(0) - tensorB.get(0)
        })
        .slice(0, k)
        .reduce((prev, tensor) => {
            return prev + tensor.get(1)
        }, 0) / k
}

let { features, labels, testFeatures, testLabels } = loadCSV('kc_house_data.csv', {
    shuffle: true,
    splitTest: 10,
    dataColumns: ['lat', 'long', 'sqft_lot', 'sqft_living'],
    labelColumns: ['price']
})

features = tf.tensor(features)
labels = tf.tensor(labels)

testFeatures.forEach((testPoint, i) => {
    const result = knn(features, labels, tf.tensor(testPoint), 10)
    const err = (testLabels[i][0] - result) / testLabels[i][0] * 100
    console.log('Error', err)
})


