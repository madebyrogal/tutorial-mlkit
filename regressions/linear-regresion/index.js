require('@tensorflow/tfjs-node')
const plot = require('node-remote-plot')
const loadCSV = require('../load-csv')
const LeanearRegression = require('./linear-regrestion')

let { features, labels, testFeatures, testLabels } = loadCSV('../data/cars.csv', {
    shuffle: true,
    splitTest: 50,
    dataColumns: ['horsepower', 'weight', 'displacement'],
    labelColumns: ['mpg'],
})

const regression = new LeanearRegression(features, labels, {
    learningRate: 0.1,
    iterations: 3,
    batchSize: 10,
})

regression.train()
const r2 = regression.test(testFeatures, testLabels)

plot({
    x: regression.mseHistory.reverse(),
    xLabel: 'MSE',
})
console.log('R2 is: ', r2)

regression.prediction([
    [120, 2, 380],
    // [135, 2.1, 420],
]).print()
