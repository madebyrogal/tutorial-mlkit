const tf = require('@tensorflow/tfjs')

class LinearRegression {
    constructor(features, labels, options) {
        this.features = this.processFeatures(features)
        this.labels = tf.tensor(labels)
        this.weights = tf.zeros([this.features.shape[1], 1])
        this.mseHistory = []
        
        this.options = Object.assign({
            learningRate: 0.1,
            iterations: 1000, // max training iteration
        }, options)
    }
    
    train() {
        const batchQuantity = Math.floor(this.features.shape[0] / this.options.batchSize)
        
        for(let i = 0; i < this.options.iterations; i++) {
            for(let j = 0; j < batchQuantity; j++) {
                const startIndex = j * this.options.batchSize
                const { batchSize } = this.options
                const featureSlice = this.features.slice([startIndex, 0], [batchSize, -1])
                const labelSlice = this.labels.slice([startIndex, 0], [batchSize, -1])
                
                this.gradientDescent(featureSlice, labelSlice)
            }
            
            this.recordMSE()
            this.updateLearningRate()
        }
    }
    
    gradientDescent(features, labels) {
        const currentGuess = features.matMul(this.weights)
        const differences = currentGuess.sub(labels);
        const slopes = features
            .transpose()
            .matMul(differences)
            .div(features.shape[0])
        
        this.weights = this.weights.sub(slopes.mul(this.options.learningRate));
    }
    
    prediction(observations) {
        return this.processFeatures(observations).matMul(this.weights)
    }
    
    test(testFeatures, testLabels) {
        testFeatures = this.processFeatures(testFeatures)
        testLabels = tf.tensor(testLabels)
        
        const prediction = testFeatures.matMul(this.weights)
        
        const res = testLabels.sub(prediction)
            .pow(2)
            .sum()
            .get()
        const tot = testLabels.sub(testLabels.mean()).pow(2).sum().get()
        
        return 1 - res / tot
    }
    
    processFeatures(features) {
        features = tf.tensor(features)
        
        if (this.mean && this.variance) {
            features = features.sub(this.mean).div(this.variance.pow(1/2))
        } else {
            features = this.standardize(features)
        }
    
        features = tf.ones([features.shape[0], 1]).concat(features, 1);
    
        return features
    }
    
    standardize(features) {
        const { mean, variance } = tf.moments(features, 0)
        
        this.mean = mean
        this.variance = variance
        
        features = features.sub(this.mean)
        features = features.div(this.variance.pow(1/2))
    
        return features
    }
    
    recordMSE() {
        const mse = this.features
            .matMul(this.weights)
            .sub(this.labels)
            .pow(2)
            .sum()
            .div(this.features.shape[0])
            .get()
        
        this.mseHistory.unshift(mse)
    }
    
    updateLearningRate() {
        if (this.mseHistory.length < 2) {
            return
        }
        
        if (this.mseHistory[0] > this.mseHistory[1]) {
            this.options.learningRate /= 2
        } else {
            this.options.learningRate *= 1.05
        }
    }
}

module.exports = LinearRegression
